import itertools

from guid_record import GuidRecord
from transaction import Transaction


class Account(GuidRecord):
    table = 'accounts'

    def __repr__(self):
        return self.name

    @property
    def name(self):
        return self.data['name']

    @property
    def parent(self):
        return Account(self.sqlite_db, self.data['parent_guid'])

    @property
    def type(self):
        return self.data['account_type']

    @property
    def children(self):
        for child_row in self.sqlite_db.execute('select * from accounts where parent_guid=?', (self.guid,)):
            yield Account(self.sqlite_db, row=child_row)

    def get_children_recursive(self):
        children = list(self.children)
        sub_children = itertools.chain.from_iterable(child.get_children_recursive() for child in children)
        return itertools.chain(children, sub_children)

    def get_transactions(self, date1=None, date2=None):
        where_query = ''
        bind_args = (self.guid,)
        if date1:
            where_query += ' and post_date>=?'
            bind_args += (date1,)
        if date2:
            where_query += ' and post_date<?'
            bind_args += (date2,)

        query = 'select * from transactions where ' \
                'guid in (select tx_guid from splits where account_guid=? group by tx_guid)' + \
                where_query + \
                ' order by post_date'

        for tx_guid in self.sqlite_db.execute(query, bind_args):
            yield Transaction(self.sqlite_db, tx_guid[0])

    @property
    def transactions(self):
        return self.get_transactions()


