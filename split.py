from fractions import Fraction
from guid_record import GuidRecord


class Split(GuidRecord):
    table = 'splits'

    @property
    def transaction_guid(self):
        return self.data['tx_guid']

    @property
    def account_guid(self):
        return self.data['account_guid']

    @property
    def account(self):
        from account import Account
        return Account(self.sqlite_db, self.account_guid)

    @property
    def value(self):
        return Fraction(self.data['value_num'], self.data['value_denom'])

    @property
    def quantity(self):
        return Fraction(self.data['quantity_num'], self.data['quantity_denom'])

    def __repr__(self):
        return "({}) {} {:.2f} {}".format(self.guid, self.account, float(self.value), float(self.quantity))
