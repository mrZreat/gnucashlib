import sqlite3

from account import Account


class GnucashDB:
    def __init__(self, filename):
        self.sqlite_db = sqlite3.connect(filename)
        self.sqlite_db.row_factory = sqlite3.Row

    def find_account(self, name):
        data = self.sqlite_db.execute('select * from accounts where name=?', (name,)).fetchone()
        return Account(self.sqlite_db, row=data)
