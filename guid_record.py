import abc


class GuidRecord:
    def __init__(self, sqlite_db, guid=None, *, row=None):
        self.sqlite_db = sqlite_db
        if row:
            self._data = row
            self.guid = row['guid']
        elif guid:
            self.guid = guid + ''
            self._data = None
        else:
            raise NotImplementedError

    @property
    @abc.abstractmethod
    def load_data(self):
        pass

    @property
    def data(self):
        if not self._data:
            query = 'select * from {} where guid=?'.format(self.table)
            self._data = self.sqlite_db.execute(query, (self.guid,)).fetchone()
        return self._data

    def __eq__(self, other):
        return self.guid == other.guid

    def __repr__(self):
        return self.guid
