from datetime import datetime, timezone
from guid_record import GuidRecord
from split import Split


class Transaction(GuidRecord):
    table = 'transactions'

    @property
    def description(self):
        return self.data['description']

    @property
    def splits(self):
        for split_row in self.sqlite_db.execute('select * from splits where tx_guid=?', (self.guid,)):
            yield Split(self.sqlite_db, row=split_row)

    @property
    def date(self):
        dbval = self.data['post_date']
        value = datetime.strptime(dbval, "%Y%m%d%H%M%S")
        return value.replace(tzinfo=timezone.utc).astimezone(tz=None).date()

    def __repr__(self):
        return "({}) {} {}".format(self.guid, self.date, self.description)
